In Dante's Inferno, the 9th circle of Hell is known as Cocytus and it is reserved for those guilty of treachery. Within this circle, there are four concentric zones or rounds, each housing different types of traitors. The characters encountered in the 9th circle include:

1. Round 1: Caina - This round is named after Cain, who killed his brother Abel out of envy. It houses traitors to their kin.

2. Round 2: Antenora - Named after Antenor, a Trojan prince who betrayed his city to the Greeks during the Trojan War. This round contains traitors to their homeland or political party.

3. Round 3: Ptolomea - Named after Ptolemy, a captain under Alexander the Great who invited guests for a feast and then murdered them. This round holds those who betrayed their guests or hosts.

4. Round 4: Judecca - Named after Judas Iscariot, one of Jesus' disciples who betrayed him for thirty pieces of silver. This final round is reserved for those who betrayed their benefactors or masters.

It's important to note that Dante encounters various historical figures and mythological characters within these rounds as he explores the depths of Hell in his poem "The Divine Comedy."
# Wed 26 Jun 02:09:43 CEST 2024 - which characters are met in the 9th circle of hell in Dante's Inferno?